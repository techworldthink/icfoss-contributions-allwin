# Task 1 - (Software Testing – Functional Testing)
*** 
* What is Functional Testing?
* What to Test in Functional Testing?
* Functional Testing Process
* Type of Functional Testing Techniques
* Functional Testing Tools
* Functional Testing Tools (for Django based apps)

You can refer to this website for a write-up about the above things from the LoRaLink application perspective.

https://www.geeksforgeeks.org/software-testing-functional-testing/

# Task 2 - Go programming
***
* Create a web application using Go.
* Install Postgresql and Pgadmin4 locally and learn about creating databases and tables.
* Create a login page using HTML and CSS.
* Establishing a Connection to PostgreSQL with GoLang
* Make a Go application that shows data and fetch on to a HTML page without database/server.
* Fetch data from a postgresql table to an HTML table using Go.
* Write login user information to a database using Go. There is no need to implement login functionality. (Just POST login data, fetch it on the server side, and write it to a history table.)
* Find the IP of a client from the incoming request for login in the Go web application. Store the login credentials along with the IP in the table.
